package com.example.demo;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@RequestMapping("/")
	String helloworld() {
		
		return "hello world";
	
	}

	@RequestMapping("/hello/{name}")
	String myName (@PathVariable String name) {
		return "My name is: " + name;
	}
}
